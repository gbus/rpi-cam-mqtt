import unittest

import unittest.mock as mock
from rpicammqtt.loadconfig import load_config, CONFIG_FILE_NAME


class TestLoadConfig(unittest.TestCase):
    def test_when_load_config_given_non_yaml_text_then_raise_exception(self):
        # Patch get_yaml_from_file
        patch_get_yaml_from_file = mock.patch(
            "rpicammqtt.loadconfig.get_yaml_from_file"
        )
        get_yaml_from_file = patch_get_yaml_from_file.start()
        get_yaml_from_file.return_value = "text with no yaml structure"

        with self.assertRaises(RuntimeError):
            load_config(CONFIG_FILE_NAME)

        patch_get_yaml_from_file.stop()


if __name__ == '__main__':
    unittest.main()
