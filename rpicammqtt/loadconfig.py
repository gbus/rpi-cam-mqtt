"""
Configuration load
"""
from os.path import expanduser
import yaml

CONFIG_FILE_NAME = 'rpi-cam-mqtt.yaml'


def get_yaml_from_file(config_file):
    """Open config_file and parse the yaml content"""
    with open(config_file, 'r') as stream:
        return yaml.safe_load(stream)


def load_config(config_file_name):
    """Attempt to load config file from different locations"""
    no_config_paths = []
    cfile = ''
    try:
        cfile = "/etc/rpi-cam-mqtt/{}".format(config_file_name)
        conf = get_yaml_from_file(cfile)
    except FileNotFoundError:
        no_config_paths.append(cfile)
        try:
            cfile = "{}/.rpi-cam-mqtt/{}".format(
                expanduser("~"),
                config_file_name
            )
            conf = get_yaml_from_file(cfile)
        except FileNotFoundError:
            no_config_paths.append(cfile)
            try:
                cfile = "./config/rpi-cam-mqtt/{}".format(
                    config_file_name
                )
                conf = get_yaml_from_file(cfile)
            except FileNotFoundError:
                no_config_paths.append(cfile)
                raise RuntimeError(
                    "Configuration file not found. Tried:\n{}".format(
                        '\n'.join(no_config_paths)
                    )
                )

    # The yaml library can return a string if yaml syntax is not detected.
    # Returning none in this case.
    if not isinstance(conf, dict):
        raise RuntimeError(
            "The configuration file {} is not valid".format(cfile)
        )

    return conf
